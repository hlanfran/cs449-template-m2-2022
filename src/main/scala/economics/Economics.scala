import org.rogach.scallop._
import breeze.linalg._
import breeze.numerics._
import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import ujson._

package economics {

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val json = opt[String]()
  verify()
}

object Economics {
  def main(args: Array[String]) {
    println("")
    println("******************************************************")

    var conf = new Conf(args)

    val iccm7_cost=38600
    val iccm7_rent=20.4
    val iccm7_num_cores=2*14
    val iccm7_num_ram=24*64
    val cont_ram_rent=1.6*math.pow(10,-7)
    val cont_cpu_rent=1.14*math.pow(10,-6)
    val rpi4_rent=108.48
    val rpi4_power_idle=3.0
    val rpi4_power_comp=4.0
    val rpi4_core_ratio=0.25
    val rpi4_num_ram=8.0

    val energy_cost=0.25
    
    val min_renting_days=ceil(iccm7_cost/iccm7_rent)
    val cont_daily_cost=(32*cont_ram_rent+1*cont_cpu_rent)*60*60*24
    val irp_daily_idle=4*(rpi4_power_idle*24/1000*energy_cost)
    val irp_daily_comp=4*(rpi4_power_comp*24/1000*energy_cost)
    val fix_costs=4*rpi4_rent
    val days_idle = ceil(fix_costs/(cont_daily_cost-irp_daily_idle))
    val days_computing = ceil(fix_costs/(cont_daily_cost-irp_daily_comp))
    val rpi_icc7=floor(iccm7_cost/rpi4_rent)
    val ratio_ram=rpi_icc7*rpi4_num_ram/(iccm7_num_ram)
    val ratio_cpu=rpi_icc7*rpi4_core_ratio/(iccm7_num_cores)
    
    // Save answers as JSON
    def printToFile(content: String,
                    location: String = "./answers.json") =
      Some(new java.io.PrintWriter(location)).foreach{
        f => try{
          f.write(content)
        } finally{ f.close }
    }
    conf.json.toOption match {
      case None => ;
      case Some(jsonFile) => {

        val answers = ujson.Obj(
          "E.1" -> ujson.Obj(
            "MinRentingDays" -> ujson.Num(min_renting_days) // Datatype of answer: Double //1893
          ),
          "E.2" -> ujson.Obj(
            "ContainerDailyCost" -> ujson.Num(cont_daily_cost), //0.540864
            "4RPisDailyCostIdle" -> ujson.Num(irp_daily_idle), //0.072
            "4RPisDailyCostComputing" -> ujson.Num(irp_daily_comp), //0.096
            "MinRentingDaysIdleRPiPower" -> ujson.Num(days_idle), //926.0
            "MinRentingDaysComputingRPiPower" -> ujson.Num(days_computing) //976.0 
          ),
          "E.3" -> ujson.Obj(
            "NbRPisEqBuyingICCM7" -> ujson.Num(rpi_icc7), //355.0
            "RatioRAMRPisVsICCM7" -> ujson.Num(ratio_ram), //1.8489583333333333
            "RatioComputeRPisVsICCM7" -> ujson.Num(ratio_cpu)  //29.258241758241756
          )
        )

        val json = write(answers, 4)
        println(json)
        println("Saving answers in: " + jsonFile)
        printToFile(json, jsonFile)
      }
    }

    println("")
  } 
}

}
