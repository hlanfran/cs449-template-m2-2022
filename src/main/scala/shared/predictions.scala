package shared

import breeze.linalg._
import breeze.numerics._
import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.SparkContext
import scala.collection.mutable.MutableList

package object predictions
{
  // ------------------------ For template
  case class Rating(user: Int, item: Int, rating: Double)

  def timingInMs(f : ()=>Double ) : (Double, Double) = {
    val start = System.nanoTime() 
    val output = f()
    val end = System.nanoTime()
    return (output, (end-start)/1000000.0)
  }

  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

  def mean(s :Seq[Double]): Double =  if (s.size > 0) s.reduce(_+_) / s.length else 0.0

  def std(s :Seq[Double]): Double = {
    if (s.size == 0) 0.0
    else { 
      val m = mean(s)
      scala.math.sqrt(s.map(x => scala.math.pow(m-x, 2)).sum / s.length.toDouble) 
    }
  }


  def load(path : String, sep : String, nbUsers : Int, nbMovies : Int) : CSCMatrix[Double] = {
    val file = Source.fromFile(path)
    val builder = new CSCMatrix.Builder[Double](rows=nbUsers, cols=nbMovies) 
    for (line <- file.getLines) {
      val cols = line.split(sep).map(_.trim)
      toInt(cols(0)) match {
        case Some(_) => builder.add(cols(0).toInt-1, cols(1).toInt-1, cols(2).toDouble)
        case None => None
      }
    }
    file.close
    builder.result()
  }

  def loadSpark(sc : org.apache.spark.SparkContext,  path : String, sep : String, nbUsers : Int, nbMovies : Int) : CSCMatrix[Double] = {
    val file = sc.textFile(path)
    val ratings = file
      .map(l => {
        val cols = l.split(sep).map(_.trim)
        toInt(cols(0)) match {
          case Some(_) => Some(((cols(0).toInt-1, cols(1).toInt-1), cols(2).toDouble))
          case None => None
        }
      })
      .filter({ case Some(_) => true
                 case None => false })
      .map({ case Some(x) => x
             case None => ((-1, -1), -1) }).collect()

    val builder = new CSCMatrix.Builder[Double](rows=nbUsers, cols=nbMovies)
    for ((k,v) <- ratings) {
      v match {
        case d: Double => {
          val u = k._1
          val i = k._2
          builder.add(u, i, d)
        }
      }
    }
    return builder.result
  }

  def partitionUsers (nbUsers : Int, number_partitions : Int, replication : Int) : Seq[Set[Int]] = {
    val r = new scala.util.Random(1337)
    val bins : Map[Int, collection.mutable.ListBuffer[Int]] = (0 to (number_partitions-1))
       .map(p => (p -> collection.mutable.ListBuffer[Int]())).toMap
    (0 to (nbUsers-1)).foreach(u => {
      val assignedBins = r.shuffle(0 to (number_partitions-1)).take(replication)
      for (b <- assignedBins) {
        bins(b) += u
      }
    })
    bins.values.toSeq.map(_.toSet)
  }

// special mention to https://github.com/scalanlp/breeze/blob/master/math/src/main/scala/breeze/linalg/CSCMatrix.scala

  //global function

  def mae_knn(test: CSCMatrix[Double], predictor : (Int, Int) => Double): Double = {
    val maeVal = test.activeIterator.foldLeft((0.0, 0.0))((acc, elem) => {
      elem._2 match {
        case 0.0 => acc
        case _ => (acc._1 + (predictor(elem._1._1, elem._1._2) - elem._2).abs, acc._2 + 1)
      }
    })
    return maeVal._1 / maeVal._2
  }

  //3

  //Scale function
  def scale(rating : Double, mean : Double) : Double = {
    if(rating > mean){
      return 5 - mean
    }else if(rating < mean){
      return mean - 1
    }else{
      return 1
    }
  }

  //Normalize the ratings
  def normalize(rating : Double, mean : Double) : Double = {
    return (rating - mean)/scale(rating,mean)
  }

  //compute the average for all the users
  def global_average(ratings : CSCMatrix[Double]) : Double = {
    var sum = 0.0
    var count = 0.0
    //iterate over elements of the matrix of user ratings
    for ((k,v) <- ratings.activeIterator) {
      if(v != 0){
        val row = k._1
        val col = k._2
        sum = sum + v
        count += 1.0
      }
    }
    return sum/count
  }

  //compute averages for every user
  def compute_user_averages(ratings : CSCMatrix[Double]) : Array[Double] = {
    
    //keep the user scores
    var users_scores_sum : Array[Double] = Array.fill(ratings.rows)(0.0)
    //count the number of rating per user
    var users_number_of_ratings : Array[Double] = Array.fill(ratings.rows)(0.0)


    for ((k,v) <- ratings.activeIterator) {
      val row = k._1
      //add score
      users_scores_sum(row) += v
      //add a user count
      users_number_of_ratings(row) += 1
    }  

    for(i <- 0 to (users_scores_sum.length - 1)){
      //compute the fraction
      users_scores_sum(i) = (users_scores_sum(i)/users_number_of_ratings(i))
    }

    return users_scores_sum
  }

  //eq2 for all users and ratings
  def normalized_ratings(ratings : CSCMatrix[Double], averages : Array[Double]) : CSCMatrix[Double] = {

    //matrix of normalized ratings
    var normalized_ratings = new CSCMatrix.Builder[Double](ratings.rows,ratings.cols).result
    
    for((k,v) <- ratings.activeIterator){
      //add normalized rating
      normalized_ratings(k._1,k._2) = normalize(v,averages(k._1))
    }

    return normalized_ratings
  }

  //eq 9
  def preprocessed_ratings(ratings : CSCMatrix[Double]) : DenseMatrix[Double] = {

    //matrix of preprocessed ratings
    var preprocessed_ratings = new CSCMatrix.Builder[Double](ratings.rows,ratings.cols).result

    //compute the norms of the ratings
    val norms = sqrt(sum(ratings.map(x => (x * x)).toDense, Axis._1))

    for((k,v) <- ratings.activeIterator){
      //compute preprocessed ratings according to the formula in the project
      preprocessed_ratings(k._1,k._2) = v / norms(k._1)
    }
    return preprocessed_ratings.toDense
  }

  //eq 10
  def similarities(ratings : DenseMatrix[Double]) : DenseMatrix[Double] = {
    //returnr the cosine similarity
    return ratings * ratings.t
  }

  //eq 7
  def compute_item_deviations(ratings : CSCMatrix[Double], normalized_ratings : CSCMatrix[Double], similarities: DenseMatrix[Double]) : DenseMatrix[Double] = {
    //get indexes of relevant users
    val indexes = ratings.toDense.map(x => if (x != 0.0) 1.0 else 0.0) 
    //compute items deviations based on relevant users
    val itemDevs = (similarities * normalized_ratings.toDense) /:/ (abs(similarities) * indexes)
    return itemDevs.map(x => if (!x.isNaN()) x else 0.0)
  }

  //take k nearest users by similarity excluding itself
  def knn(user : Int, k: Int, similarities : DenseMatrix[Double]) : Array[Int] = {
    return argtopk(similarities(user,::).t,k+1).toArray.filter(x=>x!=user)
  }

  def predict_knn_similarity(similarities : DenseMatrix[Double], k : Int): DenseMatrix[Double] = {
    for (x <- 0 to similarities.rows - 1){
      //compute best similarities for user x
      val index_user = knn(x, k, similarities)
      for(y <- 0 to similarities.cols - 1) {
        // if the user y is not part of the best similar users, set its score to 0
        if (!index_user.contains(y)) similarities(x,y) = 0.0
      }
    }
    return similarities
  }

   def predict(user_average : Double, item_dev : Double) : Double = {
     //predict score according to the formula in the project
    return user_average + item_dev * scale(user_average + item_dev, user_average)
  }

  def predictor_knn(ratings: CSCMatrix[Double], k : Int) : (Int, Int) => Double = {
    val user_average = compute_user_averages(ratings)
    val normalized_ratings_ = normalized_ratings(ratings, user_average)
    val preprocessed_ratings_ = preprocessed_ratings(normalized_ratings_)
    val similarities_ = similarities(preprocessed_ratings_)
    val similarities_knn = predict_knn_similarity(similarities_ , k)
    val item_deviations = compute_item_deviations(ratings, normalized_ratings_, similarities_knn)

    //return the predictor function
    return (u : Int, i : Int) => predict(user_average(u), item_deviations(u,i))
  }

  

  //4

  def distributed_knn(preprocessed_ratings : DenseMatrix[Double], k : Int, sc: SparkContext) : DenseMatrix[Double] = {
    //new rating matrix
    val new_ratings = new CSCMatrix.Builder[Double](preprocessed_ratings.rows, preprocessed_ratings.rows)

    //broadcast variable
    val broadcast = sc.broadcast(preprocessed_ratings)

    //topk procedure
    val topk_res = sc.parallelize(0 until preprocessed_ratings.rows).mapPartitions(iterator => 
      {
      iterator.map(user=>{
        //get broadcast variable
        val ratings = broadcast.value

        //get similarities
        val similarities = ratings * ratings.t(::,user)

        //get best k user similar
        val res=argtopk(similarities,k+1).toArray.filter(x=>x!=user)

        //map to matrix form
        val res_form = res.map(user_2 => (user,user_2,similarities(user_2)))
        res_form
      })}).collect()
    
    //add collected elements to the new matrix
    for(i <- 0 until topk_res.size)
      for (j<-0 until topk_res(i).size)
        new_ratings.add(topk_res(i)(j)._1,topk_res(i)(j)._2,topk_res(i)(j)._3)
        
    return new_ratings.result().toDenseMatrix
  }

  def distributed_knn_predictor(ratings: CSCMatrix[Double], k: Int, spark_context: SparkContext): (Int, Int) => Double = {
    val user_average = compute_user_averages(ratings)
    val normalized_ratings_ = normalized_ratings(ratings, user_average)
    val preprocessed_ratings_ = preprocessed_ratings(normalized_ratings_)
    val similarities_knn = distributed_knn(preprocessed_ratings_ , k, spark_context)
    val item_deviations = compute_item_deviations(ratings, normalized_ratings_, similarities_knn)

    return (u : Int, i : Int) => predict(user_average(u), item_deviations(u,i))
  }


//5

  def distributed_knn_approximate(preprocessed_ratings : DenseMatrix[Double], k : Int, spark_context: SparkContext, number_partitions : Int, replication : Int) : DenseMatrix[Double] = {
    //new ratings matrix
    val new_ratings = new CSCMatrix.Builder[Double](rows=preprocessed_ratings.rows, cols=preprocessed_ratings.rows)

    //user partitions
    val users_partition = partitionUsers (preprocessed_ratings.rows, number_partitions,replication)
    
    //distribute the variable among workers
    val broadcast = spark_context.broadcast(preprocessed_ratings)

    //topk procedure
    val approximate_topk = spark_context.parallelize(users_partition).map(partition_iterator => {

      //Get back broadcasted value
      val ratings = broadcast.value

      //Get the users by order of the matrix of ratings
      val sorted_users = partition_iterator.toSeq.sorted

      //get the ratings of this partition
      val partition_ratings = ratings(sorted_users, ::).toDenseMatrix

      //compute similarities of the users in this partition
      val similarities = partition_ratings * partition_ratings.t
      
      //associate index of similarities to the true index of the matrix
      val partition_index = (0 until similarities.rows).zip(partition_iterator.toArray.sorted).toMap
      
      //get most k similar user in the partition
      val topk = (0 until similarities.rows).toList.map(u => similarities(u, ::).t.toArray.zip(sorted_users).sortBy(- _._1).slice(1, k+1).map(v => (u, v._2, v._1))).flatten 

      //map the user to the real entries in the matrix
      topk.map{
        case (u, v, s) => (partition_index(u), v, s)
        }
    }).collect()
    
    //get the best k similar users accross all partitions
    val group = approximate_topk.flatten.groupBy(x => x._1).map(x => x._2.map(y => (y._2, y._3)).toArray.distinct.sortWith(_._2 > _._2).slice(0, k).map(y => (x._1, y._1, y._2))).flatten

    //add values to the matrix
    for( x <- group){
      new_ratings.add(x._1,x._2,x._3)
    }

    return new_ratings.result.toDenseMatrix
  }


  def distributed_knn_predictor_approximate(ratings: CSCMatrix[Double], k: Int, spark_context: SparkContext,  number_partitions : Int, replication : Int): (Int, Int) => Double = {
      val user_average = compute_user_averages(ratings)
      val normalized_ratings_ = normalized_ratings(ratings, user_average)
      val preprocessed_ratings_ = preprocessed_ratings(normalized_ratings_)
      val similarities_knn = distributed_knn_approximate(preprocessed_ratings_ , k, spark_context, number_partitions, replication)
      val item_deviations = compute_item_deviations(ratings, normalized_ratings_, similarities_knn)

      return (u : Int, i : Int) => predict(user_average(u), item_deviations(u,i))
    }
}




