package test.optimizing

import breeze.linalg._
import breeze.numerics._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.BeforeAndAfterAll
import shared.predictions._
import test.shared.helpers._

class OptimizingTests extends AnyFunSuite with BeforeAndAfterAll {
  
   val separator = "\t"
   val train2Path = "data/ml-100k/u2.base"
   val test2Path = "data/ml-100k/u2.test"
   var train2 : CSCMatrix[Double] = null
   var test2 : CSCMatrix[Double] = null

   override def beforeAll {
       // For these questions, train and test are collected in a scala Array
       // to not depend on Spark
       train2 = load(train2Path, separator, 943, 1682)
       test2 = load(test2Path, separator, 943, 1682)
   }

   // Provide tests to show how to call your code to do the following tasks.
   // Ensure you use the same function calls to produce the JSON outputs in
   // the corresponding application.
   // Add assertions with the answer you expect from your code, up to the 4th
   // decimal after the (floating) point, on data/ml-100k/u2.base (as loaded above).
   test("kNN predictor with k=10") { 
    val predictor_knn_ = predictor_knn(train2, 10)
    val mae = mae_knn(test2, predictor_knn_) 

    val user_average = compute_user_averages(train2)
    val normalized_ratings_ = normalized_ratings(train2, user_average)
    val preprocessed_ratings_ = preprocessed_ratings(normalized_ratings_)
    val similarities_ = similarities(preprocessed_ratings_)
    val res_similarity = predict_knn_similarity(similarities_ , 10)

     // Similarity between user 1 and itself
     assert(within(res_similarity(0,0), 0.0, 0.0001))
 
     // Similarity between user 1 and 864
     assert(within(res_similarity(0,863), 0.24232304952129619, 0.0001))

     // Similarity between user 1 and 886
     assert(within(res_similarity(0,885), 0.0, 0.0001))

     // Prediction user 1 and item 1
     assert(within(predictor_knn_(0,0), 4.319093503763853, 0.0001))

     // Prediction user 327 and item 2
     assert(within(predictor_knn_(326,1), 2.6994178006921192, 0.0001))

     // MAE on test2
     assert(within(mae, 0.8287277961963542, 0.0001)) 
   } 
}
